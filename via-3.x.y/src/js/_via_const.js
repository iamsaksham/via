'use strict'

// definition of contants that are used by all modules
const _VIA_USER_ROLE = { 'ADMIN':1, 'ANNOTATOR':2, 'REVIEWER':3 };
const _VIA_STORE_TYPE = { 'LOCALSTORAGE':1, 'COUCHDB':2 };

var _VIA_SVG_NS = "http://www.w3.org/2000/svg";
var _VIA_FLOAT_FIXED_POINT = 3; // floats are stored as 3 decimal places
